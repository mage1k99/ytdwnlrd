$(document).ready(function(){
	$('[data-bs-hover-animate]')
		.mouseenter( function(){ var elem = $(this); elem.addClass('animated ' + elem.attr('data-bs-hover-animate')) })
		.mouseleave( function(){ var elem = $(this); elem.removeClass('animated ' + elem.attr('data-bs-hover-animate')) });
		$(".dwnld-btn").click(function (){
			$(".progressx").css({"visibility": "visible"})
			$(".dwnld-btn").prop("disabled",true)
			$.ajax({
				type : "POST",
				url: "/download_ready",
				data: JSON.stringify({"yt_url":""+$("#url_id").val(),"qty_video":$(".col-3 > select:nth-child(1)").val()}),
				success: function(data){
					console.log(data);
					$(".progressx").css({visibility: "hidden"})
					$(".thumbna").attr("src",data.thumbnail)
					$(".video_name").html(data.video_title)
					$("#download-url").attr("download",data.video_title).attr("href",data.video)
					$(".dwnl-content").css({visibility:"visible"})
					$(".dwnld-btn").prop("disabled",false)
				},
				fail: function(data){
					console.log(data);
					$(".progressx").css({visibility: "hidden"})
					$(".dwnl-content").css({visibility:"hidden"})
					$(".dwnld-btn").prop("disabled",false)
				},
				contentType: "application/json",
        		dataType: 'json'
			})
		});
		console.clear();
});