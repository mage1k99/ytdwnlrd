from flask import Flask, render_template, request, send_from_directory, flash, jsonify
from pytube import YouTube
import os
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/download_ready', methods=['POST'])
def download_page_route():
    request_data = request.get_json()
    response_json = {}
    video_url = request_data["yt_url"]
    video_qty = request_data["qty_video"]
    video_stream = YouTube(video_url)
    stream = video_stream.streams.filter(file_extension='mp4').order_by('resolution')

    video_file_name = ""
    if video_qty == "low":
        video_file_name = stream.first().download()
    elif video_qty == "medium":
        video_file_name = stream[len(stream) // 2].download()
    else:
        video_file_name = stream.desc().first().download()
    if video_file_name != "":
        current_folder_name = os.getcwd().split(os.path.sep)[::-1][0]
        print(video_file_name)
        source_path = video_file_name.split(current_folder_name + os.path.sep)[1]
        destination_path = "static"+os.path.sep+"video_dist"+os.path.sep + video_file_name.split(current_folder_name + os.path.sep)[1]
        os.rename(source_path, destination_path)
        response_json.update({"video_title":video_stream.title,"thumbnail":video_stream.thumbnail_url,"video":destination_path})
        return jsonify(response_json)
    flash("Error While Downloading the Video")
    return "Unable to Download"

@app.route('/favicon.ico')
def favicon():    
    return send_from_directory("static"+os.path.sep+"assets"+os.path.sep+"img","baseline_cloud_download_white_18dp.png")

if __name__ == '__main__':
    #Run the Server in 127.0.0.1 as 0.0.0.0 will not run properly in windows
    app.run(host='0.0.0.0', port=3600, debug=False)