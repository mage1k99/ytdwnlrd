# YTDWNLDR

### Simple Youtube Downloader with minimal UI and No Ads.

---

## ❓ How to Use it ?

* Clone or Download the Project from the [repo](https://gitlab.com/mage1k99/ytdwnlrd)

* Create a Virtual Enviroment for Python3
    
    * `python3 -m venv env`
    * `source env/bin/activate` (If you're using Bash in Linux.)

* Install the python Dependencies.
    
    * `pip install -r requirements.txt`

    * `python app.py`

* Open `0.0.0.0:3600` in browser.

---
### How does it Work ?
* Flask Runs as Server.
* When A youtube link is pasted and Download Button is clicked,
A AJAX POST Request is sent to the Server.

* The Server uses a API which Returns a List of Streams.

* Each Stream Contains meta-data about the video.

* The File is saved and a JSON Response is send as Response to the AJAX Request made.

* The Video title, Link, Thumbnail is Set and Displayed to user for his Download.

---
## This is a Simple Implemenatation Done.

## PS

* If you're try running in windows, Change Host from [0.0.0.0 to 127.0.0.1](https://gitlab.com/mage1k99/ytdwnlrd/blob/66918222c004821de9f59730ca533fa324b035a6/app.py#L42)